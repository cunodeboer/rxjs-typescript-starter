import { range } from "rxjs"
import { filter, map } from "rxjs/operators"

const obs = range(1, 10)
    .pipe(
        filter(n => n % 2 === 1),
        map(n => n + n)
    )


obs.subscribe(x => document.getElementById('output').innerHTML += `${x} `)
obs.subscribe(x => console.log(x))
